# Principes de la conteneurisation

## Le chroot

Par la sécurité, on souhaitais isoler un service réseaux du reste du système. En cas de corruption de ce service, le reste du système ne peu plus être corrompu a son tour. On a mis en place le chroot.

Le chroot pour CHange ROOT ou, **change root directory**, est une fontcionalité du kernel permettant d'isolé un processus dans une sous arboressance. Pour ce processus, la racine de l'arboressances / est limité à la base de la sous arbo qui lui est dédiée.

Exemple :

Un serveur ftp "chrooté" sur /srv/ftp/chroot/ vera le contenu du dossier chroot comme la racine /

Dans ce dossier chroot, on aura un copie restreinte et limité au sctict nécessaire de l'arboressance / :

```bash
# tree -dL 1 /srv/ftp/chroot/
/srv/ftp/chroot/
├── bin
├── dev
├── etc
├── home
├── lib
├── proc
├── root
├── run
├── sbin
├── srv
├── sys
├── tmp
├── usr
└── var
```

Dans le chroot, les fonctionalités étaient limités. Exemple par defaut le dossier /proc etait vide de ce fait la commande ps ne fonctionnais pas à l'intérieur du chroot. le dossier /dev était aussi limité à très peu d'entrée.

## La conteneurisation

La conteneurisation est arrivé dans la continuité du chroot d'une part, pour completer l'isolation de processsus en restregnant le moins possible les fonctionalité. Puis pour proposer une solution de virtualisation légère d'autre part.

### OpenVZ

OpenVZ, a été la première solution complète de conteneurisation sous linux (a ce que je sais aujourd'hui).
Cette solution proposait au départ via des patchs du kernel (modification du noyau linux standard) à la fois d'un bon niveau d'isolation et très peu de restriction des fonctionalité système. Dans la "VZ" on avais des cartes réseaux, les pid etait translaté (le pid de init à l'intérieur de la VZ est 1 alors que c'est un processus standard sur le serveur)

### LXC

LXC ou "linux container" est un système de virtualisation basé sur l'isolation de processus (cgroup) et les espaces de nommage(namespace). **Il est intégré au kernel linux.**

Le "système" virtualisé n'utilise pas son propre kernel comme dans les solutions de virtualisation standard ; mais dispose d'une visibilité restreinte ou modifié du seul kernel tournant sur le système. Chaque container ne vois que ses processesus avec un pid qui est translaté, dispose de carte réseaux virtuelle et fonctionne dans une arborescance dédié.

### Docker

Docker est un outils en ligne de commande associé à un daemon, permettant de manipuler les containers. Il offre dans la continuité des précédent plus d'isolation, de souplesse et de facilité d'utilisation à la conteneurisation.

La logique sous docker change. Les containers ne sont pas des machine virtuel mais des container applicatifs. Nous avons un container par application.
Ainsi Un "lamp" sera contruit en général avec au minimum deux containers, un container apache qui porte le serveur web et l'application ainsi que le module et les bibliothèque php puis un second container gérant la base de données mysql.

## classification par niveau de virtualisation

![fig-x-virt-level](./images/virtu-level.png)
